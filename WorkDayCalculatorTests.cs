﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTest
{
    [TestClass]
    public class WorkDayCalculatorTests
    {

        [TestMethod]
        public void TestNoWeekEnd()
        {
            DateTime startDate = new DateTime(2021, 12, 1);
            int count = 10;

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, null);

            Assert.AreEqual(startDate.AddDays(count - 1), result);
        }

        [TestMethod]
        public void TestNormalPath()
        {
            DateTime startDate = new DateTime(2021, 5, 21);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2021, 5, 23), new DateTime(2021, 5, 25))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 5, 28)));
        }

        [TestMethod]
        public void TestWeekendAfterEnd()
        {
            DateTime startDate = new DateTime(2021, 5, 21);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[2]
            {
                new WeekEnd(new DateTime(2021, 5, 23), new DateTime(2021, 5, 25)),
                new WeekEnd(new DateTime(2021, 5, 29), new DateTime(2021, 5, 29))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 5, 28)));
        }

        [TestMethod]
        public void TestOneDayWeekEnd()
        {
            DateTime startDate = new DateTime(2021, 5, 10);
            int count = 9;
            WeekEnd[] weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2021, 5, 12), new DateTime(2021, 5, 12))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.AreEqual(new DateTime(2021, 5, 19), result);
        }

        [TestMethod]
        public void TestWeekEndsOnIntersection()
        {
            DateTime startDate = new DateTime(2021, 5, 10);
            int count = 1;
            WeekEnd[] weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2021, 5, 10), new DateTime(2021, 5, 13))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.AreEqual(new DateTime(2021, 5, 14), result);
        }

        [TestMethod]
        public void TestSLastWorkDay()
        {
            DateTime startDate = new DateTime(2021, 5, 10);
            int count = 2;
            WeekEnd[] weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2021, 5, 11), new DateTime(2021, 5, 13))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.AreEqual(new DateTime(2021, 5, 14), result);
        }

        [TestMethod]
        public void TestBasicWeekends()
        {
            DateTime startDate = new DateTime(2021, 5, 22);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[3]
            {
                new WeekEnd(new DateTime(2021, 5, 21), new DateTime(2021, 5, 23)),
                new WeekEnd(new DateTime(2021, 5, 26), new DateTime(2021, 5, 27)),
                new WeekEnd(new DateTime(2021, 6, 1), new DateTime(2021, 6, 3))
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 5, 30)));
        }

        [TestMethod]
        public void TestBasicWeekends2()
        {
            DateTime startDate = new DateTime(2021, 5, 23);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[2]
            {
                new WeekEnd(new DateTime(2021, 5, 21), new DateTime(2021, 5, 23)),
                new WeekEnd(new DateTime(2021, 5, 26), new DateTime(2021, 5, 27)),
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 5, 30)));
        }

        [TestMethod]
        public void TestExcludedWeekends()
        {
            DateTime startDate = new DateTime(2021, 5, 21);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[2]
            {
                new WeekEnd(new DateTime(2021, 5, 15), new DateTime(2021, 5, 16)),
                new WeekEnd(new DateTime(2021, 5, 18), new DateTime(2021, 5, 22)),
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 5, 27)));
        }

        [TestMethod]
        public void FailedTest()
        {
            DateTime startDate = new DateTime(2021, 4, 21);
            int count = 5;
            WeekEnd[] weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2021, 4, 23), new DateTime(2021, 4, 25)),
            };

            DateTime result = new WorkDayCalculator().Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2021, 4, 28)));
        }
    }
}
