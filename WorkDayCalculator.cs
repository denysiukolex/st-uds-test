﻿using System;

namespace CSharpTest
{
    public class WorkDayCalculator : IWorkDayCalculator
    {
        public DateTime Calculate(DateTime startDate, int dayCount, WeekEnd[] weekEnds)
        {
            if (weekEnds == null)
                return startDate.AddDays(dayCount - 1);

            foreach (var weekend in weekEnds)
            {
                if (startDate > weekend.EndDate)
                    continue;

                TimeSpan workPeriod = weekend.StartDate.Subtract(startDate);

                int weekendDays = weekend.EndDate.Subtract(weekend.StartDate).Days + 1;

                if (workPeriod.Days < 0)
                {
                    startDate = weekend.EndDate.AddDays(1);
                    continue;
                }

                if (workPeriod.Days < dayCount)
                {
                    startDate = startDate.AddDays(workPeriod.Days + weekendDays);
                }
                else
                {
                    startDate = startDate.AddDays(dayCount - 1);
                }

                dayCount -= workPeriod.Days;
            }

            if (dayCount > 0)
            {
                startDate = startDate.AddDays(dayCount - 1);
            }


            return startDate;


        }
    }
}
